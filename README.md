# README #

This project represents top abstraction layer over the
[StromaV](https://github.com/CrankOne/StromaV) library (and, optionally,
implementats some of its interfaces).

### What is this repository for? ###

This project:
* Can not be used standalone, without StromaV and certain interface
implementations.
* Under continious development

More info is to be added soon.
