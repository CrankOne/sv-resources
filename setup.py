# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from setuptools import setup
from pip.req import parse_requirements

reqsLst = parse_requirements('./requirements.txt')  # session=False

setup(
    name='sVresources',
    version = '0.1.dev',
    author = 'Renat R. Dusaev',
    author_email = 'crank@qcrypt.org',
    description = ( "Python applications for NA64 resources server."),
    packages=[  'sVresources.utils',
                'sVresources'],
    package_dir={
                'sVresources.utils' : 'utils',
                'sVresources' : 'sVresources',
            },
    include_package_data=True,
    install_requires=[str(ir.req) for ir in reqsLst],
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Topic :: Scientific/Engineering :: Physics",
        "License :: OSI Approved :: MIT License",
    ],
    scripts = ['sV-resources-srv'],
    long_description="""\
A toolkit to be deployed on resources server. Provides interface for
centralized management of dedicated storages for metadata, geometry and
calibration data.

Requires middleware bindings, ext.GDML and castlib2 to be deployed in
target environment."""
)
