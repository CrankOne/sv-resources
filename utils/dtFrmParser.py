# -*- coding: utf-8 -*-
# Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function
import re, json
import dpath.util

rxsKey = r'(?P<key>[^\W\[\]]+)'
rxsEntry = r'(?P<primaryKey>[^\W]+)(?P<secondaryKeys>(\[' \
         + rxsKey \
         + r'\])*)\W*'

rxKey = re.compile(rxsKey)
rxEntry = re.compile(rxsEntry)

def jQDT_form2dict( frmDct, listsAsDict=True ):
    """
    This function performs processing of jQuery's DataTable incoming POST
    requests. They're usually stored in form-fields and obey the following
    format:
        <field>[<key1>][<key2>]...[<keyn>] = value
    Where key1, key2, ... keyn may be ether string key, or an integer
    corresponding to dictionary or to list index.
    """
    res = {}
    for k, v in frmDct.iteritems():
        m = rxEntry.match( k )
        if not m:
            continue
        mdct = m.groupdict()
        if not 'secondaryKeys' in mdct.keys():
            res[mdct['primaryKey']] = v
        elif not listsAsDict:
            fullPath = [mdct['primaryKey']]
            for sk in re.finditer( rxKey, mdct['secondaryKeys'] ):
                k = sk.groupdict()['key']
                try:
                    dpath.util.get(res, fullPath)
                except KeyError:
                    dpath.util.new(res, fullPath, [] if k.isdigit() else {})
                fullPath.append(int(k) if k.isdigit() else k)
            dpath.util.new(res, fullPath, v)
        else:
            fullPathStr = mdct['primaryKey']
            for sk in re.finditer( rxKey, mdct['secondaryKeys'] ):
                fullPathStr += '/' + sk.groupdict()['key']
            dpath.util.new(res, fullPathStr, v)
    return res

if "__main__" == __name__:
    #
    # A little test:
    rs = jQDT_form2dict( {
            'columns[2][search][regex]' : False,
            'columns[2][search][value]' : None,
            'columns[2][search][regex]' : False,
        } )
    print( json.dumps(rs, indent=4, sort_keys=True) )
    print( rs )

