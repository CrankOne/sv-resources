# -*- coding: utf-8 -*-
# Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function
from sVresources import get_app
from flask import Blueprint, render_template, url_for, g

bp = Blueprint( 'sVresources', __name__,
                template_folder='templates',
                static_folder='static')

@bp.route('/')
def index_view():
    return render_template('layouts/main.html',
                            loadedBlueprints=get_app().loadedBlueprints)

@bp.route('/about/')
def about_view():
    return render_template('pages/about.html')

# Helper function
def has_no_empty_params(rule):
    defaults = rule.defaults if rule.defaults is not None else ()
    arguments = rule.arguments if rule.arguments is not None else ()
    return len(defaults) >= len(arguments)


@bp.route('/routes/')
def routes():
    getLinks = []
    postLinks = []
    for rule in get_app().url_map.iter_rules():
        # Filter out rules we can't navigate to in a browser
        # and rules that require parameters
        if "GET" in rule.methods:
            if has_no_empty_params(rule):
                url = url_for(rule.endpoint, **(rule.defaults or {}))
                getLinks.append((url, rule.endpoint))
            else:
                getLinks.append((rule.rule, "(endpoint unresolved)"))
        elif "POST" in rule.methods:
            if has_no_empty_params(rule):
                url = url_for(rule.endpoint, **(rule.defaults or {}))
                postLinks.append((url, rule.endpoint))
            else:
                postLinks.append((rule.rule, "(endpoint unresolved)"))
    # links is now a list of url, endpoint tuples
    return render_template( 'pages/routes.html',
                        getLinks=getLinks,
                        postLinks=postLinks )

@bp.app_errorhandler(404)
def page_not_found(e):
    return render_template('errors/404.html'), 404

sVresources_Blueprint = bp

