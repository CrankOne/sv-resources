# -*- coding: utf-8 -*-
# Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

import yaml, os
from flask import Flask as BaseFlask, \
                  Config as BaseConfig
from flask import g
#from flask_socketio import SocketIO
from werkzeug.routing import BaseConverter
from castlib2.db import initialize_database
from castlib2.logs import gLogger
from importlib import import_module
from functools import wraps as f_wraps
from celery import Celery

#
# Special exceptions
class InvalidUsage(Exception):
    status_code = 400
    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv

class RegexConverter(BaseConverter):
    """
    Helper class performing regex-based URL resolution.
    """
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]

class Config(BaseConfig):
    """Flask config enhanced with a `from_yaml` method."""
    def from_yaml(self, config_file):
        env = os.environ.get('sV_RESOURCES_SERVER_MODE', None)
        if not env:
            raise LookupError( "No environment variable "
                "\"sV_RESOURCES_SERVER_MODE\" set. Unable to obtain "
                "configuration section to run with." )
        self['ENVIRONMENT'] = env.lower()
        with open(config_file) as f:
            c = yaml.load(f)
        c = c['flask-app-conf'][env]
        for key in c.iterkeys():
            if key.isupper():
                self[key] = c[key]

class Flask(BaseFlask):
    """
    Extended version of `Flask` that implements custom config class.
    """
    
    def make_config(self, instance_relative=False):
        root_path = self.root_path
        if instance_relative:
            root_path = self.instance_path
        return Config(root_path, self.default_config)

    def load_blueprints(self, bps):
        loadedBlueprints = []
        from sVresources.views import sVresources_Blueprint as thisBP
        self.register_blueprint(thisBP)
        for bpString, bpCfg in bps.iteritems():
            bp = None
            try:
                bp = import_module(bpString + '.sVbp.views')
            except ImportError as e:
                gLogger.warning(
                        'Failed to import blueprint from \"%s\".'%bpString )
                gLogger.exception( e )
                loadedBlueprints.append( (bpString, 'fail') )
            else:
                self.register_blueprint(bp.sVresources_Blueprint)
                loadedBlueprints.append( (bpString, 'ok') )
            if bpCfg:
                if hasattr(bp, "configure"):
                    bp.configure( **bpCfg )
                else:
                    gLogger.warning( "Blueprint \"%s\" has not configure() " \
                        "entry point."%bpString )
            else:
                if hasattr(bp, "configure"):
                    bp.configure()
            #if hasattr( bp, 'sVresources_WSEvents' ):
            #    for eventName, v in bp.sVresources_WSEvents.iteritems():
            #        self.get_socketio().on_event( eventName,
            #                v[0], namespace=v[1] )
            #        #print( ':: ', eventName, v[1] )  # TODO: ws map?
        self.loadedBlueprints = loadedBlueprints
        return loadedBlueprints

    def get_celery(self):
        if hasattr(self, 'celery'):
            return self.celery
        celery = Celery(self.import_name,
                        backend=self.config['CELERY_RESULT_BACKEND'],
                        broker=self.config['CELERY_BROKER_URL'])
        celery.conf.update(self.config)
        TaskBase = celery.Task
        class ContextTask(TaskBase):
            abstract = True
            def __call__(self, *args, **kwargs):
                with self.app_context():
                    return TaskBase.__call__(self, *args, **kwargs)
        celery.Task = ContextTask
        self.celery = celery
        return celery

    #def get_socketio(self):
    #    if hasattr(self, 'socketio'):
    #        return self.socketio
    #    self.socketio = SocketIO(self)
    #    return self.socketio

def with_application_context():
    def inner(func):
        @f_wraps(func)
        def wrapper(*args, **kwargs):
            with get_app().app_context():
                return func(*args, **kwargs)
        return wrapper
    return inner

def get_app():
    """
    Returns current flask application instance. Utilizes a lot of customized
    stuff: DB initialization (with castlib2 ORM bindings), conditional
    blueprints load and more.
    """
    def _create_app():
        # - Init flask:
        app = Flask(__name__)
        configPath = os.environ.get('sV_RESOURCES_CONFIG_FILE', None)
        if not configPath:
            raise LookupError( "No environment variable "
                "\"sV_RESOURCES_CONFIG_FILE\" set. Unable to obtain "
                "configuration instance to run." )
            #configPath = os.path.join(app.root_path, '../instance/server.yml')
        app.config.from_yaml( configPath )
        app.url_map.converters['regex'] = RegexConverter
        # - Load configuration:
        env = os.environ.get('sV_RESOURCES_SERVER_MODE', 'development')
        with open(configPath) as yamlF:
            cfg = yaml.load(yamlF)
        # - Init database:
        app.dbCfg = cfg['database-conf'][env]
        app.dbCfgPos = app.dbCfg.pop('\\args')
        dbS = initialize_database( app.dbCfgPos,
                                   engineCreateKWargs=app.dbCfg )
        # - Init blueprints:
        app.load_blueprints( cfg['blueprints'][env] )
        app.config.sitename = cfg['sitename']
        app.config.siteurl = cfg['siteurl']
        app.config.author = cfg['author']
        app.config.authorEmail = cfg['author-email']
        @app.teardown_appcontext
        def shutdown_session(exception=None):
            dbS.remove()
        return app

    from flask import current_app
    try:
        return current_app._get_current_object()
    except RuntimeError:
        return _create_app()

