# -*- coding: utf-8 -*-
# Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

from functools import wraps as f_wraps
from flask import jsonify, g, request
from sVresources.AJAX_dynDict import JSONDynDict

class ViewRestrictions(object):
    """
    A general-purpose restriction decorator. Should be
    applied for views that are require authorized access or supposed to
    responce on AJAX requests.
    """
    def __init__(self,  minRole=None,
                        jInRq=None,
                        jOutRq=None,
                        serviceTag=None,
                        notes=[]):
        self.minimalRole        = minRole
        self.jsonInputRequired  = jInRq
        self.jsonOutRequired    = jOutRq
        self.serviceTag         = serviceTag
        self.notes = notes

    def __call__(self, f):
        @f_wraps(f)
        def decorated_function(*args, **kwargs):
            #from .accounts.models import UserRole
            #user = flogin.current_user
            outDict = getattr(g, 'jDict', None)
            if outDict is None and (self.jsonInputRequired or self.jsonOutRequired):
                outDict = g.jDict = JSONDynDict()
            if self.jsonInputRequired:
                if not request.json:
                    outDict.add_error("Request didn't bear a JSON data.")
            #if self.minimalRole and ( (user is None) or
            #    (not user.is_authenticated() or user.is_anonymous()) or
            #    not user.get_id() ):
            #    if not self.jsonOutRequired:
            #        session['loginNotes'] = jdumps(self.notes)
            #        return redirect(url_for('accounts.login_view',
            #                                next=request.url))
            #    else:
            #        out_dict().add_error('Authorization required.')
            #        if len(self.notes) > 0:
            #            out_dict().add_notification(self.notes)
            #if self.minimalRole and UserRole.ORDINARY != self.minimalRole:
            #    if (self.minimalRole == UserRole.SERVICE_ADM and
            #        user.role == UserRole.SERVICE_ADM ):
            #        abort(501)  # TODO: service admins
            #    if user.role < self.minimalRole:
            #        abort(401)
            res = f(*args, **kwargs)
            if res is None and self.jsonOutRequired:
                return outDict.j()
            return res
        return decorated_function

