# -*- coding: utf-8 -*-
# Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

from flask import jsonify, g
from sVresources import InvalidUsage

class JSONDynDict(dict):
    """
    A python-dict extender for dynamic (JSON) requests.
    It kind a simplifies some special JSON-response fields
    like errors and warnings that are expected by most
    AJAX functions.
    """
    def __init__(self, *args, **kwargs):
        super(JSONDynDict, self).__init__(*args, **kwargs)

    def append_msgs(self, msgGroup, msg):
        """
        Implements message addition to
        errors/earnings/notifications or user-defined
        arrays.
        """
        if not msgGroup in self.keys():
            if not isinstance(msg, list):
                self[msgGroup] = [msg]
            else:
                self[msgGroup] = msg
        else:
            if not isinstance(msg, list):
                self[msgGroup].append(msg)
            else:
                self[msgGroup] += msg

    def add_error(self, errorMsg):
        """ 'errors' array shortcut. """
        self.append_msgs( 'errors', errorMsg )

    def add_warning(self, warnMsg):
        """ 'warnings' array shortcut. """
        self.append_msgs( 'warnings', warnMsg )

    def add_notification(self, ntfMsg):
        """ 'notifications' array shortcut. """
        self.append_msgs( 'notifications', ntfMsg )

    def has_errors(self):
        if 'errors' in self.keys() and len(self['errors']):
            return True
        return False

    def j(self):
        return jsonify(self)

def out_dict():
    """
    Returns JSON outDict, if it is available in current context.
    """
    outDict = getattr(g, 'jDict', None)
    if outDict is None:
        raise InvalidUsage( 'Can not retrieve a outDict object for current context.' )
    return outDict

